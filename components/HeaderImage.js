import React, {Component} from 'react';
import {Image} from 'react-native';

class HeaderImage extends Component {
    constructor(props){
        super(props);
        this.state = {searchTerm: ''};
    }

    render(){
        return(
            <Image
                style={this.props.imageStyle}
                source={require('../images/100montaditosCover.jpg')}
            />
        )
    }
}

export default HeaderImage;