import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Image, Alert, ScrollView} from 'react-native';
import { SearchBar, ListItem, Icon } from 'react-native-elements';
import SearchInput, { createFilter } from 'react-native-search-filter';
const KEYS_TO_FILTERS = ['location', 'address'];

class MainSearchBar extends Component {
  constructor(props){
    super(props);
    this.state = {searchTerm: ''};
  }

  searchUpdated(term){
    this.setState({searchTerm : term})
  }

  renderScheduleInfo(){
    return <View style={{borderTopColor:'gray', borderTopWidth:0.5, flexDirection :'row'}}>
              <Text style={{paddingTop:7, color:'green', fontWeight:'bold'}}>Aberto</Text>
              <Text style={{paddingTop:7, marginLeft:7}}>Horário: 09:00 - 23:00</Text>
            </View>
  }

  renderDistanceInfo(address, distance){
    return <View style={{flexDirection :'row', paddingBottom:5}}>
            <Icon name='location-on'/>
            <Text style={styles.distance}> {distance} </Text>
            <Text style={{paddingLeft:5, paddingTop:3}}> {address} </Text>
           </View>
  }

  renderItems(establishments){
    return(
      <View>
      {establishments.map((l, i) => (
        <View>
          <ListItem
            key={i}
            /**leftAvatar={{ source: require('../images/100montaditosLogo.png') }}**/
            title={l.location}
            titleStyle={styles.title}
            subtitle={
              <View>
                {this.renderDistanceInfo(l.address, l.distance)}
                {this.renderScheduleInfo()}
              </View>
            }
            chevron={true}
            onPress={()=> Alert.alert('Show Establishment Page')}
            containerStyle={{backgroundColor:'#F0F0F0'}}
          />
          <View style={{height:12, backgroundColor:'white'}}/>
        </View>
      ))}
      </View>
    )
  }

  render(){
    const filteredEstablishments = establishments.filter(createFilter(this.state.searchTerm, KEYS_TO_FILTERS))
    return(
      <View style={{flex:1}}>
        <SearchBar
          platform="android"
          cancelIcon={{ type: 'font-awesome', name: 'chevron-left' }}
          placeholder='Search location'
          onChangeText={(term) => {this.searchUpdated(term)}} 
          onClear={() => {this.searchUpdated('')}} 
          containerStyle={{}}/>
        <ScrollView style={styles.scroll}>
          {this.renderItems(filteredEstablishments)}
        </ScrollView>
      </View>
    )
  }
}

export default MainSearchBar;

const styles = StyleSheet.create({
  title:{
    paddingBottom : 7,
    fontWeight:'bold',
    fontSize:18
  },
  scroll: { 
    height : 300
  },
  listItem: {
    flexDirection :'row'
  },
  distance: {
    fontWeight: 'bold',
    paddingTop:3
  }
});

const establishments = [
    {
      location: 'Braga',
      address: 'Rua Nova de Santa Cruz, 14',
      distance: '214 M'
    },
    {
      location: 'Coimbra',
      address: 'Avenida Sá Da Bandeira, 114',
      distance: '20 Km'
    },
    {
      location: 'Figueira da Foz',
      address: 'Rua Bernardo Lopes, 45',
      distance: '34 Km'
    },
    {
      location: 'Leiria',
      address: 'Rua Monte de Aviz, 9',
      distance: '51 Km'
    },
    {
      location: 'Lisboa - Campo Grande',
      address: 'Rua Ernesto De Vasconcelos, 193',
      distance: '56 Km'
    },
    {
      location: 'Lisboa - Dom Luís',
      address: 'Praça Dom Luís I, 10',
      distance: '67 Km'
    },
    {
      location: 'Lisboa - Oriente',
      address: 'Rua Pimenta, 103',
      distance: '83 Km'
    },
    {
      location: 'Odivelas',
      address: 'Rua Pulido Valente, 7',
      distance: '96 Km'
    },
    {
      location: 'Lisboa - Duque de Avila',
      address: 'Av. Duque de Ávila, 47',
      distance: '102 Km'
    },
    {
      location: 'Almancil',
      address: 'Avenida do Algarve',
      distance: '120 Km'
    },
    {
      location: 'Vila Nova de Gaia - Arrábida Shopping',
      address: 'Praceta de Henrique Moreira, 244',
      distance: '154 Km'
    },
    {
      location: 'Porto',
      address: 'Estrada De Circunvalação, 7612',
      distance: '181 Km'
    },
    {
      location: 'Almada Forum',
      address: 'Estrada Do Caminho Municipal, 1011',
      distance: '194 Km'
    }
  ]
  