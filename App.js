import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Image, Alert, ScrollView} from 'react-native';
import MainSearchBar from './components/MainSearchBar.js'
import HeaderImage from './components/HeaderImage.js'

type Props = {};
export default class App extends Component<Props> {

  render() {
    return (
      <View style={styles.mainContainer}>
        <HeaderImage imageStyle={styles.headerImage}/>
        <MainSearchBar/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    justifyContent: 'flex-start',
    /**alignItems: 'center',**/
    backgroundColor: '#F5FCFF',
  },
  headerImage: {
    alignSelf : 'center',
    resizeMode : 'cover',
    height: 200,
    width: 412
  }
});